@ui @alertFrameAndWindows
Feature: Alert, Frame and Windows

  @alerts
  Scenario: As a user, I am able to see alert when click to alert button
    Given I am on the "alerts" page
    When I click on "Click Button to see alert " button
    Then I should see "You clicked a button" text in alert

