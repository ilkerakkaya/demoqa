@ui @interactions
Feature: Interactions

  @dragAndDrop
  Scenario: As a user I should be able to drag and drop
    Given I am on the "droppable" page

    When I move "Drag me" box to the "Drop here" box
    Then I should see "Dropped!" text
