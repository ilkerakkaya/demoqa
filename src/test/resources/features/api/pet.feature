@api @pet @smoke
Feature: Pet

  Background:
    Given The endpoint is "pet/"

  @getPet @deletePet
  Scenario Outline: As a user I am able to get the pet I want
    Given A pet with following details:
      | id   | name   | status   |
      | <id> | <name> | <status> |

    When I request the pet which has ID "<id>"

    And Response body should have following pet details:
      | id   | name   | status   |
      | <id> | <name> | <status> |

    Examples:
      | id  | name  | status      |
      | 999 | mavis | coming soon |