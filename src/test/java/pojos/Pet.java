package pojos;

public class Pet {
    /**
     *  {
     *         "id": 3,
     *             "name": "Simo",
     *             "photoUrls": [
     *
     *     ],
     *         "tags": [
     *
     *     ],
     *         "status": "NonNon"
     *     }
     *
     */
    private int id;
    private String name;
    private String status;

    public Pet(String id, String name, String status) {
        this.id = Integer.parseInt(id);
        this.name = name;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
