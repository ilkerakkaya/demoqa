package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.Driver;

public class AlertFrameAndWindowsPage {
    private final WebDriver driver = Driver.getDriver();

    public void clickAlertBtn(String text){
        String clickMeButton = "//*[*[text()='%s']]/following-sibling::*/*[@id='alertButton']";
        driver.findElement(By.xpath(String.format(clickMeButton, text))).click();
    }

    public String getAlertMessage(){
        Alert alert = driver.switchTo().alert();
        String msg = alert.getText();
        alert.accept();

        return msg;
    }
}
