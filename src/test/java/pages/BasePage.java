package pages;

import io.cucumber.java.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.ConfigReader;
import utilities.Driver;

public class BasePage {

    private final WebDriver driver = Driver.getDriver();

    private final String baseLocator = "//*[text()='%s']";

    public void goTo(String page) {
        String baseUrl = ConfigReader.getProperty("ui_base_url");
        driver.get(baseUrl + page);
    }

    public WebElement getElementWithText(String text){
        return driver.findElement(By.xpath(String.format(baseLocator, text)));
    }
}
