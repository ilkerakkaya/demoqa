package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.Driver;
import utilities.SeleniumUtils;

public class WidgetsPage {
    private final WebDriver driver = Driver.getDriver();

    private final WebElement toolTipButton = driver.findElement(By.id("toolTipButton"));

    public void hoverToTooltipBtn(){
        SeleniumUtils.hoverOver(toolTipButton);
    }

}
