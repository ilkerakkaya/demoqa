package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.Driver;
import utilities.SeleniumUtils;

public class InteractionsPage {

    private final WebDriver driver = Driver.getDriver();

    private final WebElement droppable = driver.findElement(By.xpath("//*[@id='simpleDropContainer']/*[@id='droppable']"));
    private final WebElement draggable = driver.findElement(By.xpath("//*[@id='simpleDropContainer']/*[@id='draggable']"));

    public void dragAndDrop(){
        SeleniumUtils.dragAndDrop(draggable, droppable);
    }

}
