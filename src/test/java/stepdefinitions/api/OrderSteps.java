package stepdefinitions.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.hamcrest.Matchers.*;
import pojos.Order;
import utilities.ApiUtils;

import java.util.HashMap;

public class OrderSteps {

    private Order order;

    @Given("(I want to create )an order with following data:")
    public void i_want_to_create_an_order_with_following_data(Order orderDt) {
        order = orderDt;
    }

    @When("I create an order")
    public void iCreateAnOrder() {
        ApiUtils.post(mapOrder());
    }

    @And("Response body should have following order details:")
    public void responseBodyShouldHaveFollowingOrderDetails(Order orderDt) {
        ApiUtils.response.body(
                "id", equalTo(orderDt.getId()),
                "petId", equalTo(orderDt.getPetId()),
                "quantity", equalTo(orderDt.getQuantity()),
                "shipDate", containsString(orderDt.getShipDate()),
                "status", equalTo(orderDt.getStatus()),
                "complete", equalTo(orderDt.isComplete())
        );
    }

    @Then("I see order is deleted successfully")
    public void i_see_order_is_deleted_successfully() {

    }

    @After(value = "@deleteOrder")
    public void deleteOrder(){
        ApiUtils.delete(String.valueOf(order.getId()));
    }

    private HashMap mapOrder() {
        ObjectMapper obj = new ObjectMapper();
        return obj.convertValue(order, HashMap.class);
    }

}
