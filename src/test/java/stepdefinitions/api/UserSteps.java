package stepdefinitions.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

import static org.hamcrest.Matchers.*;

import pojos.PostResponse;
import pojos.User;
import utilities.ApiUtils;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class UserSteps {

    private User user;

    @Given("I want to create a user with following data:")
    public void i_want_to_create_a_user_with_following_data(User userDt) {
        user = userDt;
    }

    @When("I do POST request for user")
    public void i_do_post_request_for_user() {
        ApiUtils.post(mapUser());
    }

    @And("Response body should have following user details:")
    public void responseBodyShouldHaveFollowingUserDetails(PostResponse responseDt) {
        ApiUtils.response.body(
                "code", equalTo(responseDt.getCode()),
                "type", equalTo(responseDt.getType()),
                "message", equalTo(responseDt.getMessage())
        );

    }

    @After(value = "@deleteUser")
    public void deleteUser() {
        ApiUtils.delete(user.getUsername());
    }

    private HashMap mapUser() {
        ObjectMapper obj = new ObjectMapper();
        return obj.convertValue(user, HashMap.class);
    }
}
