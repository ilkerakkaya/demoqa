package stepdefinitions.api;

import io.cucumber.java.en.Given;
import static io.restassured.RestAssured.*;


public class BaseSteps {

    @Given("The endpoint is {string}")
    public void the_endpoint_is(String endpoint) {
        basePath = endpoint;
    }

}
