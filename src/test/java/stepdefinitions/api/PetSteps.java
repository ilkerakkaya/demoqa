package stepdefinitions.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import static org.hamcrest.Matchers.*;
import pojos.Pet;
import utilities.ApiUtils;

import java.util.HashMap;

public class PetSteps {

    private Pet pet;

    @Given("A pet with following details:")
    public void aPetWithFollowingDetails(Pet petDt) {
        pet = petDt;
        ApiUtils.post(mapPet());
    }

    @When("I request the pet which has ID {string}")
    public void i_request_the_pet_which_has_id(String petId) {
        ApiUtils.get(petId);
    }

    @And("Response body should have following pet details:")
    public void responseBodyShouldHaveFollowingPetDetails(Pet petDt) {
        ApiUtils.response.body(
                "id", equalTo(petDt.getId()),
                        "name", equalTo(petDt.getName()),
                "status", equalTo(petDt.getStatus())

        );
    }

    @After(value = "@deletePet")
    public void deletePet(){
        ApiUtils.delete(String.valueOf(pet.getId()));
    }

    private HashMap mapPet(){
        ObjectMapper obj = new ObjectMapper();
        return obj.convertValue(pet, HashMap.class);
    }
}
