package stepdefinitions.ui;

import io.cucumber.java.en.When;
import pages.InteractionsPage;

public class InteractionsSteps extends InteractionsPage {

    @When("I move \"Drag me\" box to the \"Drop here\" box")
    public void i_move_box_to_the_box() {
        dragAndDrop();
    }
}
