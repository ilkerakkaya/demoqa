package stepdefinitions.ui;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.AlertFrameAndWindowsPage;

public class AlertFrameAndWindowsSteps extends AlertFrameAndWindowsPage {

    @When("I click on {string} button")
    public void i_click_on_button(String elText) {
        clickAlertBtn(elText);
    }

    @Then("I should see {string} text in alert")
    public void i_should_see_text_in_alert(String expectedText) {
        Assert.assertEquals("Alert message is not as expected",
                expectedText, getAlertMessage()
        );
    }
}
