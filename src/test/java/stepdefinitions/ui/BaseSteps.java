package stepdefinitions.ui;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.BasePage;
import utilities.Driver;
import utilities.SeleniumUtils;

public class BaseSteps extends BasePage {

    @Given("I am on the {string} page")
    public void i_am_on_the_page(String page) {
        goTo(page);
    }

    @Then("I should see {string} text")
    public void i_should_see_text(String expectedText) {
        Assert.assertTrue("No such text",
                SeleniumUtils.isVisible(getElementWithText(expectedText))
        );
    }

    @After(value = "@ui")
    public void tearDown(){
        Driver.closeDriver();
    }
}
