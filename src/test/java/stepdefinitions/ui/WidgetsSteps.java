package stepdefinitions.ui;

import io.cucumber.java.en.When;
import pages.WidgetsPage;

public class WidgetsSteps extends WidgetsPage {

    @When("I hover over \"Hover me to see\" button")
    public void i_hover_over_button() {
        hoverToTooltipBtn();
    }
}
