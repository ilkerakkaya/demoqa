package utilities;

import static io.restassured.RestAssured.*;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class ApiUtils {
    public static ValidatableResponse response;

    private static RequestSpecification requestSpecification;
    private static final int SUCCESS_CODE = 200;

    private static RequestSpecification request(){
        //set base uri
        baseURI = ConfigReader.getProperty("api_base_uri");

        //set generic request spec
        return given().headers("Content-Type", "application/json");
    }

    public static void get(String endpoint){
        response = request().when().get(endpoint).then().statusCode(SUCCESS_CODE);
    }

    public static void post(Object requestBody){
        response = request().body(requestBody).when().post().then().statusCode(SUCCESS_CODE);
    }

    public static void delete(String endpoint){
        response = request().given().when().delete(endpoint).then().statusCode(SUCCESS_CODE);
    }
}

