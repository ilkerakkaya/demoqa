package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class SeleniumUtils {

    private static final WebDriver driver = Driver.getDriver();
    private static final Actions actions = new Actions(driver);

    public static boolean isVisible(WebElement el){
        try{
            return  el.isDisplayed();
        }catch(NoSuchElementException e){
            return false;
        }
    }

    public static void dragAndDrop(WebElement srcEl, WebElement destEl){
        actions.dragAndDrop(srcEl, destEl).build().perform();
    }

    public static void hoverOver(WebElement el){
        actions.moveToElement(el).build().perform();
    }

}
